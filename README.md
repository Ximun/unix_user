## Unix user

This roles:
  * creates specific unix group for a given user
  * creates user with common preferences (shell, comments, gid, uid)
  * copy given ssh keypairs for this user if given

This role is a frontend to [ansible user module](https://docs.ansible.com/ansible/latest/modules/user_module.html).

Tested on **debian 10** with **ansible >= 2.7**.

## Role parameters

| name                  | value       | mandatory_value         | description                 |
| ----------------------|-------------|-------------------------|-----------------------------|
| user                  | map         | yes                     | see following examples      |
| user.login            | string      | yes                     | username                    |
| user.uid              | integer     | yes                     | UID to set for the user     |
| user.comment          | string      | yes                     | description (aka GECOS) of user account |
| user.shell            | string      | yes                     | shell for user              |
| user.group.name       | string      | yes                     | name of unix group          |
| user.group.id         | integer     | yes                     | GID to set for the group    |
| user.ssh              | map         | yes if group is defined | ssh key parameters          |
| user.ssh.key          | string      | yes if ssh is defined   | filename of private ssh key |
| user.ssh.hash         | string      | yes if ssh is defined   | hash of the private ssh key |
| user.ssh.pub          | string      | yes if ssh is defined   | filename of public ssh key  |
| user.sudo_filename    | string      | no                      | filename of sudo file       |


## Using this role

### ansible galaxy
Add the following in your *requirements.yml*.

```yaml
---
- src: https://gitlab.com/ansible-roles3/unix_user.git
  scm: git
  version: v1.4
```

### Sample playbook

```yaml
---
- hosts: all
  roles:
    - role: unix_user
      user:
        login: johndoe
        uid: 777
        group:
          name: noone
        comment: BOFH
        shell: /bin/bash
        ssh:
          key: tests/files/ssh
          hash: 690fff43537d864f0f25f60e0a4a598c43981df3
          pub: tests/files/ssh.pub
```

See file [tests/tests_unix_user](tests/tests_unix_user) for more examples.
